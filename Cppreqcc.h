/*=================================================================//
// cppreqcc.h               CPpreQCC 2.6 - Preprocessor for QuakeC //
//-----------------------------------------------------------------//
// Written using borland C++ 5 by Sergio Fuma�a Grunwaldt          //
//=================================================================//
// Program constants and function declarations. Updated 30/04/2020 //
//=================================================================*/

// Enumeration types
enum parse_type { PARSE_IFDEF, PARSE_IFNDEF };

// Functions
void ErrorExit(char *msg);
void PerformCleanUp ();
void PrintIt (char* what);
void PrintSourceWarning (char* thefile, int theline, char* what);
void PrintSourceError (char* thefile, int theline, char* what);
void PrintSourceInfo (char* thefile, int theline, char* what);
bool PutFileInQueue(char* fname);
void PutParseInQueue(parse_type type, bool causes_parse);
void ResolveParseItem();
void InvertParseItem();
void SourceErrorExit();
void ProcessFile();
bool CharIsSeparator(char tchar);
//bool CharIsNewLine(char tchar);
int GetKeywordType(int curpos);
int GetPragmaType(int curpos);
bool DirectiveWantsData(int tkeyw);
void ExecuteDirective(int curpos);
unsigned int IsDefined(char* ident);
void WriteOutChar(char data);
void PutDefineInList(char* ident, char* value);
void ExpandTargetMem();
int IsNotEmpty(char* tstr);
//bool PragmaWantsData(int tkeyw);
void ExecutePragma(int curpos);
char* CleanStr(char* tstr);
void UndefineItem(unsigned int numdefine);
unsigned int IsInDefList(char* ident);
bool IsValidEndList(unsigned int curpos);
bool IsEmptyChar(char tchar);
int GetDirectiveType(char* tkey);
char* ParseValue(char* tvalue);
char* ChangeFilename(char *fname);
void PrintFile (char* what);
void CheckUnused();
char* CleanUpPragmaStr(char* value); // January 2024
char* CleanUpIncludeStr(char* value);

// String messages
#define ST_START    "CPPreQCC v2.6 - Written by OfteN[cp] for Prozac CustomTF\nInitializing...\n"
#define ST_SUCCESS  "Job done! Have a nice day.\nTime for compiler errors... :)\n"
#define ST_BOXERROR "wtf.. ERROR, dammit!"

// preprogs filename default - TODO: make command line param
#define ST_PREPROGS_SRC "PREPROGS.SRC"
#define ST_SEPARATORS " !*+-=)(/,.><{}\""
#define NUM_SEPARATORS 16

// Several stuff maximums
#define MAX_FILE_QUEUE    256// max files to process
#define MAX_PARSE_QUEUE    16// max depth of consecutive IFDEF or IFNDEFS
#define MAX_ST_SIZE       512//256// maximum string size
#define MAX_DEFINES      4096// was 2048

// line Scan status flags
#define SCN_STATUS_IDLE         0//write output, searching for anything
#define SCN_STATUS_KEYWORD      1//skips output, awaiting a directive or define identifier
#define SCN_STATUS_VALUE        2//skips output, awaiting some kind of value (after a define or a pragma)
#define SCN_STATUS_LIST         3//skips output, we r inside an include list
#define SCN_STATUS_PRAGMA       4//skips output, awaiting the pragma keyword
#define SCN_STATUS_DEFINE       5//skips output, we r waiting for the defined value after a IFDEF, UNDEF, DEFINE or IFNDEF
#define SCN_STATUS_PRAGMAVALUE  6//skips output, we r expecting a pragma parameter (PROGS_SRC or PROGS_DAT strings)
#define SCN_STATUS_INCLUDE      7//skips output, adds a new file at the end of the current line - January 2024

//===============================================================
// DIRECTIVES

// Supported directives
#define ST_DIR_IFDEF        "ifdef"
#define ST_DIR_IFNDEF       "ifndef"
#define ST_DIR_ELSE         "else"
#define ST_DIR_ENDIF        "endif"
#define ST_DIR_DEFINE       "define"
#define ST_DIR_UNDEF        "undef"
#define ST_DIR_PRAGMA       "pragma"
#define ST_DIR_INCLUDELIST  "includelist"
#define ST_DIR_ENDLIST      "endlist"
#define ST_DIR_INCLUDE      "include"

// directives ID // GetDirective() return values
#define DIRECTIVE_IDENT         0//not a directive, an identifier probably
#define DIRECTIVE_IFDEF         1
#define DIRECTIVE_IFNDEF        2
#define DIRECTIVE_ELSE          3
#define DIRECTIVE_ENDIF         4
#define DIRECTIVE_DEFINE        5
#define DIRECTIVE_UNDEF         6
#define DIRECTIVE_PRAGMA        7
#define DIRECTIVE_INCLUDELIST   8
#define DIRECTIVE_ENDLIST       9
#define DIRECTIVE_INCLUDE       10

//================================================
// PRAGMAS

// Supported pragmas
#define ST_PRAGMA_COMPILE           "COMPILE_THIS_FILE"
#define ST_PRAGMA_NOCOMPILE         "DONT_COMPILE_THIS_FILE"
#define ST_PRAGMA_PROGSSRC          "PROGS_SRC"
#define ST_PRAGMA_PROGSDAT          "PROGS_DAT"
#define ST_PRAGMA_CHECKREDEFS_ON    "CHECK_REDEFINES_ON"
#define ST_PRAGMA_CHECKREDEFS_OFF   "CHECK_REDEFINES_OFF"
#define ST_PRAGMA_KEEPNEWLINES_ON   "KEEP_NEWLINES_ON"
#define ST_PRAGMA_KEEPNEWLINES_OFF  "KEEP_NEWLINES_OFF"
#define ST_PRAGMA_KEEPCOMMENTS_ON   "KEEP_COMMENTS_ON"
#define ST_PRAGMA_KEEPCOMMENTS_OFF  "KEEP_COMMENTS_OFF"
#define ST_PRAGMA_CHECKUNUSED_ON    "CHECK_UNUSED_ON"
#define ST_PRAGMA_CHECKUNUSED_OFF   "CHECK_UNUSED_OFF"

// Default values for PROGS.SRC and PROGS.DAT - January 2024
#define ST_DEFAULT_PROGSSRC "PROGS.SRC"
#define ST_DEFAULT_PROGSDAT "../qwprozac.dat"
// End January 2024

// pragmas ID // GetPragma() return values
#define PRAGMA_EMPTYSTILL       -1
#define PRAGMA_UNSUPPORTED      0//means error, or unknown pragma

#define PRAGMA_COMPILE          1//implemented
#define PRAGMA_NOCOMPILE        2//implemented

#define PRAGMA_PROGSSRC         3//done in January 2024 -TODO
#define PRAGMA_PROGSDAT         4//done in January 2024 -TODO

#define PRAGMA_CHECKREDEFS_ON   5//implemented
#define PRAGMA_CHECKREDEFS_OFF  6//implemented

#define PRAGMA_KEEPNEWLINES_ON  7//TODO
#define PRAGMA_KEEPNEWLINES_OFF 8//TODO

#define PRAGMA_KEEPCOMMENTS_ON  9//TODO
#define PRAGMA_KEEPCOMMENTS_OFF 10//TODO

#define PRAGMA_CHECKUNUSED_ON   11
#define PRAGMA_CHECKUNUSED_OFF  12

//=====================================
// PARSE TYPES

//enum parse_type { PARSE_IFDEF, PARSE_IFNDEF };
//#define PARSE_IFDEF     0
//#define PARSE_IFNDEF    1

//=====================================
// Date and time values

#define COMPUTE_BUILD_YEAR \
    ( \
        (__DATE__[ 7] - '0') * 1000 + \
        (__DATE__[ 8] - '0') *  100 + \
        (__DATE__[ 9] - '0') *   10 + \
        (__DATE__[10] - '0') \
    )


#define COMPUTE_BUILD_DAY \
    ( \
        ((__DATE__[4] >= '0') ? (__DATE__[4] - '0') * 10 : 0) + \
        (__DATE__[5] - '0') \
    )


#define BUILD_MONTH_IS_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_FEB (__DATE__[0] == 'F')
#define BUILD_MONTH_IS_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define BUILD_MONTH_IS_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define BUILD_MONTH_IS_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define BUILD_MONTH_IS_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define BUILD_MONTH_IS_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define BUILD_MONTH_IS_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define BUILD_MONTH_IS_SEP (__DATE__[0] == 'S')
#define BUILD_MONTH_IS_OCT (__DATE__[0] == 'O')
#define BUILD_MONTH_IS_NOV (__DATE__[0] == 'N')
#define BUILD_MONTH_IS_DEC (__DATE__[0] == 'D')


#define COMPUTE_BUILD_MONTH \
    ( \
        (BUILD_MONTH_IS_JAN) ?  1 : \
        (BUILD_MONTH_IS_FEB) ?  2 : \
        (BUILD_MONTH_IS_MAR) ?  3 : \
        (BUILD_MONTH_IS_APR) ?  4 : \
        (BUILD_MONTH_IS_MAY) ?  5 : \
        (BUILD_MONTH_IS_JUN) ?  6 : \
        (BUILD_MONTH_IS_JUL) ?  7 : \
        (BUILD_MONTH_IS_AUG) ?  8 : \
        (BUILD_MONTH_IS_SEP) ?  9 : \
        (BUILD_MONTH_IS_OCT) ? 10 : \
        (BUILD_MONTH_IS_NOV) ? 11 : \
        (BUILD_MONTH_IS_DEC) ? 12 : \
        /* error default */  99 \
    )

#define COMPUTE_BUILD_HOUR ((__TIME__[0] - '0') * 10 + __TIME__[1] - '0')
#define COMPUTE_BUILD_MIN  ((__TIME__[3] - '0') * 10 + __TIME__[4] - '0')
#define COMPUTE_BUILD_SEC  ((__TIME__[6] - '0') * 10 + __TIME__[7] - '0')


#define BUILD_DATE_IS_BAD (__DATE__[0] == '?')

#define BUILD_YEAR  ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_YEAR)
#define BUILD_MONTH ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_MONTH)
#define BUILD_DAY   ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_DAY)

#define BUILD_TIME_IS_BAD (__TIME__[0] == '?')

#define BUILD_HOUR  ((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_HOUR)
#define BUILD_MIN   ((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_MIN)
#define BUILD_SEC   ((BUILD_TIME_IS_BAD) ? 99 :  COMPUTE_BUILD_SEC)

//========================================================================
// 2020 - Get runtime local date and time functions, to stamp on QuakeC

void GetFormattedDate();
void GetFormattedTime();