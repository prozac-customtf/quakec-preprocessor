/*================================================================//
// cppreqcc.cpp            CPpreQCC 2.6 - Preprocessor for QuakeC //
//----------------------------------------------------------------//
// Written using borland C++ 5 by S.F.Grunwaldt aka OfteN[cp]     //
//================================================================//
// Currently compatible with Microsoft Visual C++ 2015

// This program was created because the original preqcc crashed
// randomly when processing Prozac CustomTF, and i reached a 
// point it didnt run without crashing.
// So this proggy is basically a clone of the original QuakeC
// preprocessor, that was what i needed. (written from scratch)
// It is mostly a slow program, so.. optimize it! :)

It has the same pragma's and behaves the same way, except:

"#INCLUDE" directive is not supported (use INCLUDELIST)
Update from January 2024 - include directive is now supported...
"PROGS_DAT" pragma is not supported (always uses "../qwprozac.dat")
Update from January 2024 - PROGS_DAT pragma now works...
"PROGS_SRC" pragma is not supported (always uses "PROGS.SRC")
Update from January 2024 - PROGS_SRC pragma now works...
"KEEP_NEWLINES_X" pragmas are not supported (always keeps newlines)

// Updated on April 2020 with the _date _time stuff and made it 
// compilable under modern VS 2015
// Made compilable under Linux on 09/01/2023 by OfteN [cp]
//================================================================*/

#ifdef _WIN32
#include <windows.h>
#pragma warning(disable:4996)
#else
#pragma GCC diagnostic ignored "-Wwrite-strings"
#pragma GCC diagnostic ignored "-Wconversion-null"
#pragma GCC diagnostic ignored "-Wpointer-arith"
#endif


#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <float.h>

#include <time.h>

#include "Cppreqcc.h"

const char* defines_c[] =
{
// Preprocessor date-time values
"_date",
"_time",
// temporary entities
"TE_SPIKE",
"TE_SUPERSPIKE",
"TE_GUNSHOT",
"TE_EXPLOSION",
"TE_TAREXPLOSION",
"TE_WIZSPIKE",
"TE_KNIGHTSPIKE",
"TE_LAVASPLASH",
"TE_TELEPORT",
"TE_LIGHTNING1",
"TE_LIGHTNING2",
"TE_LIGHTNING3",
"TE_BLOOD",
"TE_LIGHTNINGBLOOD",
// Sound Channel of entities
"CHAN_AUTO",
"CHAN_WEAPON",
"CHAN_VOICE",
"CHAN_ITEM",
"CHAN_BODY",
"CHAN_NO_PHS_ADD",
// Sound Attenuation
"ATTN_NONE",
"ATTN_NORM",
"ATTN_IDLE",
"ATTN_STATIC",
// Contents of level areas
"CONTENT_EMPTY",
"CONTENT_SOLID",
"CONTENT_WATER",
"CONTENT_SLIME",
"CONTENT_LAVA",
"CONTENT_SKY",
// Entity light effects
"EF_BRIGHTFIELD",
"EF_MUZZLEFLASH",
"EF_BRIGHTLIGHT",
"EF_DIMLIGHT",
"EF_FLAG1",
"EF_FLAG2",
"EF_BLUE",
"EF_RED",
// Existing Items
"IT_AXE",
"IT_SHOTGUN",
"IT_SUPER_SHOTGUN",
"IT_NAILGUN",
"IT_SUPER_NAILGUN",
"IT_GRENADE_LAUNCHER",
"IT_ROCKET_LAUNCHER",
"IT_LIGHTNING",
"IT_EXTRA_WEAPON",
"IT_SHELLS",
"IT_NAILS",
"IT_ROCKETS",
"IT_CELLS",
"IT_ARMOR1",
"IT_ARMOR2",
"IT_ARMOR3",
"IT_SUPERHEALTH",
"IT_KEY1",
"IT_KEY2",
"IT_INVISIBILITY",
"IT_INVULNERABILITY",
"IT_SUIT",
"IT_QUAD",
// Behavior of solid objects
"SOLID_NOT",
"SOLID_TRIGGER",
"SOLID_BBOX",
"SOLID_SLIDEBOX",
"SOLID_BSP",
// Type of movements
"MOVETYPE_NONE",
"MOVETYPE_ANGLENOCLIP",
"MOVETYPE_ANGLECLIP",
"MOVETYPE_WALK",
"MOVETYPE_STEP",
"MOVETYPE_FLY",
"MOVETYPE_TOSS",
"MOVETYPE_PUSH",
"MOVETYPE_NOCLIP",
"MOVETYPE_FLYMISSILE",
"MOVETYPE_BOUNCE",
"MOVETYPE_BOUNCEMISSILE",
// Entity can solid take damage
"DAMAGE_NO",
"DAMAGE_YES",
"DAMAGE_AIM",
// Entity dead flag
"DEAD_NO",
"DEAD_DYING",
"DEAD_DEAD",
"DEAD_RESPAWNABLE",
// Spawnflags
"DOOR_START_OPEN",
"SPAWN_CRUCIFIED",
"PLAT_LOW_TRIGGER",
"SPAWNFLAG_NOTOUCH",
"SPAWNFLAG_NOMESSAGE",
"PLAYER_ONLY",
"SPAWNFLAG_SUPERSPIKE",
"SECRET_OPEN_ONCE",
"PUSH_ONCE",
"WEAPON_SHOTGUN",
"H_ROTTEN",
"WEAPON_BIG2",
"START_OFF",
"SILENT",
"SPAWNFLAG_LASER",
"SECRET_1ST_LEFT",
"WEAPON_ROCKET",
"H_MEGA",
"DOOR_DONT_LINK",
"SECRET_1ST_DOWN",
"WEAPON_SPIKES",
"DOOR_GOLD_KEY",
"SECRET_NO_SHOOT",
"WEAPON_BIG",
"DOOR_SILVER_KEY",
"SECRET_YES_SHOOT",
"DOOR_TOGGLE",
"FL_FLY",
"FL_SWIM",
"FL_CLIENT",
"FL_INWATER",
"FL_MONSTER",
"FL_GODMODE",
"FL_NOTARGET",
"FL_ITEM",
"FL_ONGROUND",
"FL_PARTIALGROUND",
"FL_WATERJUMP",
"FL_JUMPRELEASED",
// Network Protocol
"MSG_BROADCAST",
"MSG_ONE",
"MSG_ALL",
"MSG_INIT",
"MSG_MULTICAST",
"PRINT_LOW",
"PRINT_MEDIUM",
"PRINT_HIGH",
"PRINT_CHAT",
"MULTICAST_ALL",
"MULTICAST_PHS",
"MULTICAST_PVS",
"MULTICAST_ALL_R",
"MULTICAST_PHS_R",
"MULTICAST_PVS_R",
"SVC_SETVIEWPORT",
"SVC_SETANGLES",
"SVC_TEMPENTITY",
"SVC_KILLEDMONSTER",
"SVC_FOUNDSECRET",
"SVC_INTERMISSION",
"SVC_FINALE",
"SVC_CDTRACK",
"SVC_SELLSCREEN",
"SVC_UPDATE",
"SVC_SMALLKICK",
"SVC_BIGKICK",
"SVC_MUZZLEFLASH",
"TRUE",
"FALSE",
"RANGE_MELEE",
"RANGE_NEAR",
"RANGE_MID",
"RANGE_FAR",
"STATE_TOP",
"STATE_BOTTOM",
"STATE_UP",
"STATE_DOWN",
"VEC_ORIGIN",
"VEC_HULL_MIN",
"VEC_HULL_MAX",
"VEC_HULL2_MIN",
"VEC_HULL2_MAX",
"UPDATE_GENERAL",
"UPDATE_STATIC",
"UPDATE_BINARY",
"UPDATE_TEMP",
"AS_STRAIGHT",
"AS_SLIDING",
"AS_MELEE",
"AS_MISSILE",
NULL};


//--------------------------------------------------------------//

// CONSOLE APPLICATION HANDLE
//HANDLE hndOut;

// FILE HANDLE
//HANDLE fhnd;
FILE *fhnd;

char* memfile; // This will point to the current file placed in memory
char* targetmem; // points to the memory containing the processed data

// This will contain the size of the file currently open
//DWORD fsize;
unsigned int fsize;
//DWORD targetmempos;
int targetmempos;
//DWORD targetmemsize;
int targetmemsize;

unsigned int filecount; // counter for files included from current source code
char curfile[MAX_ST_SIZE]; // filename of current file

int flinescount; // file lines count
int glinescount; // global number of lines count

bool we_r_parsing; // Current parsing status
bool we_r_bcomented; // Are we comented out with /* */ block?
bool we_r_lcomented; // The line we r currently processing is comented with //

unsigned int q_filepos; // our current position on the file queue to be processed
unsigned int q_filenum; // the number of files on queue

struct fileitem
{
    char filename[MAX_ST_SIZE]; // name of the file
    char invoked_by[MAX_ST_SIZE]; // filename of the file where this one was included
    int on_line; // line where it was invoked
    bool compile; // should we compile this file?
};
fileitem* filequeue[MAX_FILE_QUEUE]; // array of pointers to our queue of files to process

unsigned int q_parsepos; // our current position on the parsing queue
unsigned int q_parsenum; // the number of items on queue

struct parseitem
{
    bool causes_parse; // the condition on this #IFDEF or #IFNDEF caused parsing? (excludes code?)
    parse_type type; // it was an IFNDEF or IFDEF?
    bool inversed; // are we in 'inversed' status after an ELSE ?
    int on_line; // where this parsing condition occured on source code?
};
parseitem* parsequeue[MAX_PARSE_QUEUE]; // array of pointers to our parse queue

unsigned int gdefscount; // global defines counter
unsigned int fdefscount; // file defines counter

unsigned int gbytesread;
unsigned int gbyteswritten;

struct defineitem
{
    char identifier[MAX_ST_SIZE]; // name of the define
    bool is_defined; // if false, this define identifier was 'undefined' so we should ignore it
    char value[MAX_ST_SIZE]; // the current value assigned to this define (string even for floats)
	bool is_used; // flag to determine if this define is ever used on code
	char defined_by[MAX_ST_SIZE]; // filename where it was defined
	int on_line; // line of quakeC code where this value was defined
};
defineitem* deflist[MAX_DEFINES]; // pointer to out define list array

// Pragma variables
// Preprocessor settings vars
bool check_redefines;
bool keep_newlines;
bool keep_comments;
bool check_unused;

char progs_src[MAX_ST_SIZE];
char progs_dat[MAX_ST_SIZE];

// Scanning status
int scan_status;
unsigned int scan_offset;

int curkeywtype;
int curpragmatype;

//DWORD bytesread;
int bytesread;

char curident[MAX_ST_SIZE];
char curvalue[MAX_ST_SIZE];

bool quote_state;

int parse_stack;

// Time Stats
unsigned long startticks;

// Fast mode switch
bool fastmode;

// Date and time strings
char _date[MAX_ST_SIZE];
char _time[MAX_ST_SIZE];

//=========================================================================
// Main program function

int main(int argc, char **argv)
{
    // Get current tick for stats
#ifdef _WIN32
	startticks = GetTickCount();
#endif

	// Initialize stuff
    char tstr[32]; // Temp string

	fastmode = false;

	// Get fast mode from command line
	if (argc > 1)
	{
		strncpy(tstr,argv[1],31); // _s
		tstr[31]='\0';
		
		#ifdef _WIN32
        if (_strcmpi(tstr," -fast"))
        #else
        if (strcmp(tstr," -fast"))
        #endif
			fastmode = true;
	}
				
    gdefscount = 0;
    fdefscount = 0;

    we_r_parsing = false;
    we_r_lcomented = false;
    we_r_bcomented = false;

    glinescount = 0;
    flinescount = 0;

    curfile[0] = '\0';

    memfile = NULL;
    targetmem = NULL;

	check_redefines = true;
    keep_newlines = true;
	check_unused = true;

	gbytesread = 0;
	gbyteswritten = 0;

	// 2020 Moved int i before the first loop
	int i;

    // Init our file queue array pointers to NULL
    for (i = 0; i < MAX_FILE_QUEUE; i++)
        filequeue[i] = NULL;
    // Init to NULL parse queue array pointers
    for (i = 0; i < MAX_PARSE_QUEUE; i++)
        parsequeue[i] = NULL;
	// Init to NULL pointers to defines
	for (i = 0; i < MAX_DEFINES; i++)
		deflist[i] = NULL;

    q_parsepos = 0;
    q_parsenum = 0;

    q_filepos = 0;
    q_filenum = 0;

    scan_status = SCN_STATUS_IDLE;
    // done - stuff ready

    // Get console app handle
    /*hndOut = GetStdHandle(STD_OUTPUT_HANDLE);

    // Check if it isn't valid
    if (hndOut == INVALID_HANDLE_VALUE)
    {
        MessageBox(NULL,"CPpreQCC: Can't get a valid console handle!",ST_BOXERROR,NULL);
        ExitProcess(1);
    }*/

	// Start-initialize message
    PrintIt(ST_START);

	// Report if fast mode is on
	if (fastmode)
		PrintIt("Fast mode activated.\n");
	
    // Put 'preprogs' on queue of files to process
	PutFileInQueue(ST_PREPROGS_SRC);

    // January 2024 - Add default filenames for processed SRC and DAT files
    progs_src[0] = '\0';
    strcat(progs_src, ST_DEFAULT_PROGSSRC);
    progs_dat[0] = '\0';
    strcat(progs_dat, ST_DEFAULT_PROGSDAT);

	// Get time and date globals
	GetFormattedDate();
	GetFormattedTime();

	// Put date and time as defines
	PutDefineInList("_date", _date);
	PutDefineInList("_time", _time);

    // Set 0x09 or \t Horizontal tab in windows as a define, allowing our corresponding quake character
    //PutDefineInList("CC_DR", "\t");
	
	// 2020 Moved before the int loop
	unsigned int f;

    //---------- FILE CYCLING LOOP -----------------//
    for (f=0; f<q_filenum; f++)
    {
        // Set current file string
        //strcpy_s(curfile,filequeue[q_filepos]->filename);
        strcpy(curfile, filequeue[q_filepos]->filename);

        // Get handle to file
        fhnd = fopen(curfile, "rb");//CreateFile(curfile,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,NULL,NULL);

        // If failed to open, exit
        //if (fhnd == INVALID_HANDLE_VALUE)
        if (fhnd == NULL)
        {
            if (q_filepos == 0)
                ErrorExit("Can't open 'preprogs.src' file!\n");
            else
            {
                char tempstr[MAX_ST_SIZE+21];
                tempstr[0]='\0';
                strcat(tempstr,"Can't open '"); // _s
                strcat(tempstr,curfile); // _s
                strcat(tempstr,"' file!"); // _s
                PrintSourceError(filequeue[q_filepos]->invoked_by,
                                 filequeue[q_filepos]->on_line,
                                 tempstr);
            }
        }

        // Get file size - FIXME: Won't work with 100gigabyte files and such :)
        //fsize = GetFileSize(fhnd,NULL);

        fseek(fhnd, 0, SEEK_END); // seek to end of file
        fsize = ftell(fhnd); // get current file pointer
        fseek(fhnd, 0, SEEK_SET); // seek back to beginning of file

		// Check if memory pointers are clean
		if (memfile != NULL || targetmem != NULL)
			ErrorExit("Memory error!\n");

        // Allocate memory for source and output file
        memfile = (char*)malloc(fsize + 2);
        targetmem = (char*)malloc(fsize + 2); // Expanded with realloc by MAX_ST_SIZE byte increments if needed
        if (memfile == NULL || targetmem == NULL)
            ErrorExit("Failed memory allocation!\n");

        // LOAD FILE
        //if (ReadFile(fhnd,memfile,fsize,&bytesread,NULL) == 0)
        if (fread(memfile,1,fsize,fhnd) != fsize)
        {
            if (q_filepos == 0)
                ErrorExit("Can't load 'preprogs.src' file!\n");
            else
            {
                char tempstr[MAX_ST_SIZE+21];
                tempstr[0]='\0';
                strcat(tempstr,"Can't load '");
                strcat(tempstr,curfile);
                strcat(tempstr,"' file!");
                PrintSourceError(filequeue[q_filepos]->invoked_by,
                                 filequeue[q_filepos]->on_line,
                                 tempstr);
            }
        }

        // display load message		
		if (fastmode)
		{
			PrintIt("parsing ");
			PrintIt(curfile);
			PrintIt("\n");
		}
		else
		{			
			PrintIt(curfile);
			sprintf(tstr,"%d",fsize);
			PrintIt(" (");
			PrintIt(tstr);
			PrintIt(" bytes) loaded.\n");
		}

		memfile[fsize] = '\r';
		memfile[fsize+1] = '\n';
		fsize += 2;

		// Increase bytes read var
		gbytesread += fsize;

        // Prepare vars
        targetmemsize = fsize;
        targetmempos = 0; // our pos on the output memory stream to be the final file
        scan_status = SCN_STATUS_IDLE;
        scan_offset = 0;
        curkeywtype = 0;
        curpragmatype = 0;
        flinescount = 0;
        fdefscount = 0;
        filecount = 0;
        we_r_parsing = false;
        we_r_lcomented = false;
        we_r_bcomented = false;
        quote_state = false;
        parse_stack = 0;

        // do it jimmy!
        ProcessFile();

        // Perform cycle cleanup
        if (memfile != NULL)
        {
            free(memfile);
            memfile = NULL;
        }

        // Close file
        if (fhnd != NULL)//INVALID_HANDLE_VALUE)
        {
            //CloseHandle(fhnd);
            fclose(fhnd);
            fhnd = NULL;// INVALID_HANDLE_VALUE;
        }

        // display message
        if (q_filepos == 0)
        {
            PrintIt("Pre-progs source (");
            sprintf(tstr,"%d",q_filenum);
            PrintIt(tstr);
            PrintIt(" queued files) processed.\n");
        }
        else
        {
            if (!filequeue[q_filepos]->compile)
            {
                // display processed message
                if (fastmode)
				{
					;//PrintIt(" done.\n");
				}
				else
				{
					sprintf(tstr,"%d",targetmempos);
					PrintIt(curfile);

					PrintIt(" (");
					PrintIt(tstr);
					PrintIt(" bytes) processed. ");

					sprintf(tstr,"%d",fdefscount);
					PrintIt(tstr);
					PrintIt(" defines, ");

					sprintf(tstr,"%d",flinescount+1);
					PrintIt(tstr);
					PrintIt(" lines.\n");
				}
            }
            else
            {
                char* changed_curfile = ChangeFilename(curfile);
                
                //strcpy(curfile, ChangeFilename(curfile));
                strcpy(curfile, changed_curfile);
                free(changed_curfile);

                //fhnd = CreateFile(curfile,GENERIC_WRITE,NULL,NULL,CREATE_ALWAYS,NULL,NULL);
                fhnd = fopen(curfile, "wb");
                // If failed to open, exit
                //if (fhnd == INVALID_HANDLE_VALUE)
                if (fhnd == NULL)
                {
                    if (q_filepos == 0)
                        ErrorExit("Can't get a handle to write 'preprogs.src' file!\n");
                    else
                    {
                        char tempstr[MAX_ST_SIZE+21];
                        tempstr[0]='\0';
                        strcat(tempstr,"Can't get a handle to write '");
                        strcat(tempstr,curfile);
                        strcat(tempstr,"' file!");
                        PrintSourceError(filequeue[q_filepos]->invoked_by,
                                         filequeue[q_filepos]->on_line,
                                         tempstr);
                    }
                }

                // WRITE FILE
                //if (WriteFile(fhnd,targetmem,targetmempos,&bytesread,NULL) == 0)
                if (fwrite(targetmem,1,targetmempos,fhnd) != targetmempos)
                {
                    if (q_filepos == 0)
                        ErrorExit("Can't write 'preprogs.src' file!\n");
                    else
                    {
                        char tempstr[MAX_ST_SIZE+21];
                        tempstr[0]='\0';
                        strcat(tempstr,"Can't write '");
                        strcat(tempstr,curfile);
                        strcat(tempstr,"' file!");
                        PrintSourceError(filequeue[q_filepos]->invoked_by,
                                         filequeue[q_filepos]->on_line,
                                         tempstr);
                    }
                }

                // Close file
                //if (fhnd != INVALID_HANDLE_VALUE)
                if (fhnd != NULL)
                {
                    //CloseHandle(fhnd);
                    fclose(fhnd);
                    fhnd = NULL;// INVALID_HANDLE_VALUE;
                }

                // display written message
                if (fastmode)
				{
					;
				}
				else
				{
					PrintIt(curfile);

					sprintf(tstr,"%d",targetmempos);
					PrintIt(" (");
					PrintIt(tstr);
					PrintIt(" bytes) written. ");

					sprintf(tstr,"%d",fdefscount);
					PrintIt(tstr);
					PrintIt(" defines, ");

					sprintf(tstr,"%d",flinescount+1);
					PrintIt(tstr);
					PrintIt(" lines.\n");
				}

				gbyteswritten += targetmempos;
            }

        }

        // Clean targetmem
        if (targetmem != NULL)
        {
            free(targetmem);
            targetmem = NULL;
        }

        q_filepos++;
    }
    //------ END OF FILE CYCLING LOOP -------//

	// If enabled, check unused stuff
	if (check_unused)
		CheckUnused();

    // Print global stats
    PrintIt("------------ Preprocessor Results --------------\n");
    sprintf(tstr,"%d",gdefscount);
    PrintIt(tstr);
    PrintIt(" defines, ");
    sprintf(tstr,"%d",glinescount);
    PrintIt(tstr);
    PrintIt(" lines of code in ");
    sprintf(tstr,"%d",q_filenum);
    PrintIt(tstr);
    PrintIt(" files.\n");

	// Bytes read/written stats
	sprintf(tstr, "%d bytes read and ", gbytesread);
	PrintIt(tstr);
	sprintf(tstr,"%d bytes written.\n",gbyteswritten);
	PrintIt(tstr);

    // Write progs.src
    //fhnd = CreateFile("progs.src",GENERIC_WRITE,NULL,NULL,CREATE_ALWAYS,NULL,NULL);
    //fhnd = fopen("PROGS.SRC", "wb");
    fhnd = fopen(progs_src, "wb");

    // If failed to open, exit
    if (fhnd == NULL)//INVALID_HANDLE_VALUE)
        ErrorExit("Can't get a handle to write intermediate progs source file!\n");//PROGS.SRC file!\n");

    // Writing PROGS.SRC !!
    //PrintIt("Writing \"PROGS.SRC\" file...\n");
    PrintIt("Writing \"");
    PrintIt(progs_src); 
    PrintIt("\" file...\n");

    // First line. Target progs filename
    //PrintFile("../qwprozac.dat\n");
    PrintIt("Progs DAT set to \"");
    PrintIt(progs_dat);
    PrintIt("\" filename.\n");

    PrintFile(progs_dat);
    PrintFile("\n");

    int g;
    g = 0;
    for (f = 1; f < q_filenum; f++)
    {
        if (filequeue[f]->compile)
        {
            char* changed_filename = ChangeFilename(filequeue[f]->filename);
            //strcpy(filequeue[f]->filename, ChangeFilename(filequeue[f]->filename)); // x = <-- added 2020

            strcpy(filequeue[f]->filename, changed_filename); // x = <-- added 2020

            free(changed_filename);
			
            char* clean_filename = CleanStr(filequeue[f]->filename); // PUTA 2020
            //PrintFile(filequeue[f]->filename);
            PrintFile(clean_filename);
            free(clean_filename);
            PrintFile("\n");
            g++;
        }
    }

    PrintIt(progs_src);
    PrintIt(" (");
    sprintf(tstr,"%d",g);
    PrintIt(tstr);
    PrintIt(" files included) written.\n");

    // Close file
    if (fhnd != NULL)//!= INVALID_HANDLE_VALUE)
    {
        //CloseHandle(fhnd);
        fclose(fhnd);
        fhnd = NULL;// INVALID_HANDLE_VALUE;
    }

    // Get time elapsed
#ifdef _WIN32
	unsigned long endtime = GetTickCount();
	
	//..and print it
	PrintIt("Time elapsed: ");
	sprintf(tstr,"%.2f",float(endtime - startticks) /1000);
	PrintIt(tstr);
	PrintIt(" seconds.\n------------------------------------------------\n");
#else
    PrintIt("------------------------------------------------\n");
#endif

    // Hopefully all is ok and job done... //
    if (!fastmode)
		PrintIt(ST_SUCCESS);

    PerformCleanUp();
    return 0;
}

//=================================================================================
// Char-by-char loop for processing files - File already loaded and prepared here

void ProcessFile()
{
    unsigned int k;
	unsigned int i;

	// Process LOOP for each file
    for (i=0; i<fsize; i++)
    {
        if (scan_status==SCN_STATUS_IDLE) // idle status, anything may come in
        {
            switch (memfile[i])
            {
                case '/':

                    if (i>0) // not on first char..
                    {
                        if (memfile[i-1]=='/') // and this is the second '/'..
                            we_r_lcomented = true; // we are comented out for this line
                        else if (memfile[i-1]=='*')
                            we_r_bcomented = false; // we r not block comented anymore
                    }

                    break;

                case '*':

                    if (i>0) // not on first char..
                        if (memfile[i-1]=='/' && !we_r_lcomented) // we r not block comented and it was preceeded by a '/'
                            we_r_bcomented = true; // we are comented out for this line

                    break;

                case '"':

                    if (!we_r_lcomented && !we_r_bcomented && !we_r_parsing)
                        quote_state = !quote_state;

                    break;

                case '#':

                    if (!we_r_lcomented && !we_r_bcomented) // if we arent comented out currently..
                    {
                        if (i+1 < fsize)
                        {
                            if (memfile[i+1]>=0x30 && memfile[i+1]<=0x39)
                            {
                                ; // its a built-in declaration, do nothing
                            }
                            else
                            {
                                scan_status = SCN_STATUS_KEYWORD; // change mode --->
                                scan_offset = i + 1; // <----> save pos
                            }
                        }
                    }

                    break;
            }
        }
        else if (scan_status == SCN_STATUS_KEYWORD) // expecting a keyword or define identifier
        {
            switch (memfile[i])
            {
                case '!':
                case '*':
                case '+':
                case '-':
                case '=':
                case ')':
                case '(':
                case '/':
                case '.':
                case ',':
                case '<':
                case '>':
                case '{':
                case '}':
                case '"':
                case '&':
                case '|':
				case '\\': // 2020
				case ';': // separators, GET KEYWORD, if the directive wants data, return an ERROR

                    if (scan_offset == i) // Empty keyword error
                        PrintSourceError(curfile,flinescount,"#..? Directive or identifier expected.");

                    curkeywtype = GetKeywordType(i);

                    if (DirectiveWantsData(curkeywtype))
						PrintSourceError(curfile,flinescount,"Illegal separator use in directive or identifier.");

                    ExecuteDirective(i);                    
                    break;

                case ' ': // Space or tab char, special separators
                case 0x09: // GET KEYWORD, SET NEW STATUS

                    if (scan_offset == i) // Empty keyword error
                        PrintSourceError(curfile,flinescount,"#..? Directive or identifier expected.");

                    curkeywtype = GetKeywordType(i);

                    ExecuteDirective(i); // UPDATES GLOBALS, STATUS, AND APPLY CHANGES (reads curkeywtype var)
                    break;

                case 0x0D:
                case 0x0A: // GET KEYWORD, IF VALUE NEEDED, RETURN ERROR

                    if (scan_offset == i) // Empty keyword error
                        PrintSourceError(curfile,flinescount,"#..? Directive or identifier expected.");

                    curkeywtype = GetKeywordType(i);

                    if (DirectiveWantsData(curkeywtype))
						PrintSourceError(curfile,flinescount,"Unexpected end of line.");

                    ExecuteDirective(i);
                    break;

                case '#':

                    if (scan_offset == i) // consecutive #?
                    {
					    PrintSourceWarning(curfile,flinescount,"Consecutive '#'s. (results in a single char)");
						scan_status = SCN_STATUS_IDLE;
					}
					else
						PrintSourceError(curfile,flinescount,"Invalid '#' char inside keyword.");

                    break;
            }
        }
        else if (scan_status == SCN_STATUS_PRAGMA) // expecting a pragma identifier
        {
            switch (memfile[i])
            {
                case '!':
                case '*':
                case '+':
                case '-':
                case '=':
                case ')':
                case '(':
                case '/':
                case '.':
                case ',':
                case '<':
                case '>':
                case '{':
                case '}':
                case '"':
                case '&':
                case '|':
				case '\\': // 2020
                case ';': // separators, illegal expecting a pragma, return an ERROR

                    PrintSourceError(curfile,flinescount,"Pragma identifier expected.");
                    break;

                case ' ': // Space or tab char, legal
                case 0x09: // GET KEYWORD, SET NEW STATUS

                    curpragmatype = GetPragmaType(i);

                    // Unsupported pragma?
                    if (curpragmatype == 0)
                        PrintSourceError(curfile,flinescount,"Unknown/Unsupported pragma.");

                    if (curpragmatype != PRAGMA_EMPTYSTILL) // if pragma wasnt empty..
                        ExecutePragma(i); // UPDATES GLOBALS, STATUS, AND APPLY CHANGES (reads curkeywtype var)

                    break;

                case 0x0D:
                case 0x0A: // GET KEYWORD, IF VALUE NEEDED, RETURN ERROR

                    if (scan_offset == i) // end of line just before PRAGMA
                        PrintSourceError(curfile,flinescount,"Pragma identifier expected.");

                    curpragmatype = GetPragmaType(i);

                    // Unsupported pragma?
                    if (curpragmatype == 0)
                        PrintSourceError(curfile,flinescount,"Unknown/Unsupported pragma.");

                    if (curpragmatype != PRAGMA_EMPTYSTILL) // if pragma wasnt empty..
                        ExecutePragma(i); // UPDATES GLOBALS, STATUS, AND APPLY CHANGES (reads curkeywtype var)

                    break;

                case '#':

                    PrintSourceError(curfile,flinescount,"Invalid '#' char, pragma identifier expected.");
                    break;
            }
        }
        else if (scan_status == SCN_STATUS_VALUE) // watching for data for define, parse quotes? nope..
        {
            if (i - scan_offset > MAX_ST_SIZE - 1)
                PrintSourceError(curfile,flinescount,"Value exceeds string char max (512)");

            switch (memfile[i])
            {
                case 0x0D:
                case 0x0A:

                    // Get current value string
                    for (k = 0; k < (i - scan_offset); k++)
                        curvalue[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curvalue)!=0)
                        PutDefineInList(curident,curvalue);
                    else // Use a default value for our define, as VALUE is empty
					    PutDefineInList(curident,"1\0");

                    scan_status = SCN_STATUS_IDLE;
                    break;
            }
        }
        else if (scan_status == SCN_STATUS_INCLUDE) // January 2024 - Single include's
        {
            if (i - scan_offset > MAX_ST_SIZE - 1)
                PrintSourceError(curfile, flinescount, "Filename exceeds string char maximum (512)");

            switch (memfile[i])
            {
                case 0x0D:
                case 0x0A:

                    for (k = 0; k < (i - scan_offset); k++)
                        curvalue[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curvalue) != 0)
                    {
                        //char* clean_fname = CleanStr(curvalue);

                        char* clean_fname = CleanUpIncludeStr(curvalue);

                        if (PutFileInQueue(clean_fname))
                        {
                            char tstr[MAX_ST_SIZE + 64];
                            tstr[0] = '\0';

                            strcat(tstr, "File item named \"");
                            strcat(tstr, clean_fname);
                            strcat(tstr, "\" was included.");

                            PrintSourceInfo(curfile, flinescount, tstr);
                        }

                        free(clean_fname);
                    }
                    else
                        PrintSourceError(curfile, flinescount, "Filename from an include directive expected.");
                    
                    scan_status = SCN_STATUS_IDLE;
                    memset(curvalue, 0, MAX_ST_SIZE);
                    break;
            }
        
        }
        else if (scan_status == SCN_STATUS_PRAGMAVALUE) // expecting a pragma value - January 2024
        {
            if (i - scan_offset > MAX_ST_SIZE - 1)
                PrintSourceError(curfile, flinescount, "Value exceeds string char max (512)");

            switch (memfile[i])
            {
                case 0x0D:
                case 0x0A:

                    // Get current value string
                    for (k = 0; k < (i - scan_offset); k++)
                        curvalue[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curvalue) != 0)
                    {
                        char* clean_str = CleanUpPragmaStr(curvalue);

                        char tstr[MAX_ST_SIZE + 64];
                        tstr[0] = '\0';

                        if (curpragmatype == PRAGMA_PROGSSRC)
                        {
                            strcat(tstr, "Progs SRC ouput has been set to \"");
                            strcat(tstr, clean_str);
                            strcat(tstr, "\" filename.");

                            strcpy(progs_src, clean_str);
                            PrintSourceInfo(curfile, flinescount, tstr);
                        }
                        else if (curpragmatype == PRAGMA_PROGSDAT)
                        {
                            strcat(tstr, "Progs DAT ouput has been set to \"");
                            strcat(tstr, clean_str);
                            strcat(tstr, "\" filename.");

                            strcpy(progs_dat, clean_str);
                            PrintSourceInfo(curfile, flinescount, tstr);//"Progs DAT file has been set.");
                        }
                        else
                            PrintSourceError(curfile, flinescount, "Unknown pragma type inside PRAGMAVALUE scan status!");

                        free(clean_str);
                    }
                    else 
                        PrintSourceError(curfile, flinescount, "Pragma quoted value string expected.");
                        
                    scan_status = SCN_STATUS_IDLE;
                    memset(curvalue, 0, MAX_ST_SIZE);
                    break;
            }
        }
        else if (scan_status == SCN_STATUS_LIST) // we r inside an include list
        {
            if (i - scan_offset > MAX_ST_SIZE - 1)
                PrintSourceError(curfile,flinescount,"Filename string exceeds char max (512)");

            switch (memfile[i])
            {
                case 0x0D:
                case 0x0A:

                    // Get current filename string
                    for (k = 0; k < (i - scan_offset); k++)
                        curvalue[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curvalue)!=0)
                    {
                        PutFileInQueue(curvalue);
                    }

                    scan_offset = i + 1;
                    memset(curvalue,0,MAX_ST_SIZE);

                    break;

                case '#':

                    // Check for end of list
                    if (IsValidEndList(i))
                    {
                        i = i + 7; // update our pos, as we dont want ENDLIST to be written on output file
                        scan_status = SCN_STATUS_IDLE;
                    }
                    else
                        PrintSourceError(curfile,flinescount,"Directives not supported inside INCLUDELIST.");

                    break;
            }
        }
        else if (scan_status == SCN_STATUS_DEFINE) // waiting a define identifier for a DEFINE, UNDEF, IFDEF, IFNDEF
        {
            if (i - scan_offset > MAX_ST_SIZE - 1)
                PrintSourceError(curfile,flinescount,"Identifier exceeds string char max (512)");

            switch (memfile[i])
            {
                case 0x0D:
                case 0x0A:

                    if (scan_offset == i) // end of line just before keyword
                        PrintSourceError(curfile,flinescount,"Define identifier expected.");

                    // Get current identifier string
                    for (k = 0; k < (i - scan_offset); k++)
                        curident[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curident)!=0)
                    {
                        char* clean_ident = CleanStr(curident); // PUTA 2020

                        int numdefine = IsDefined(clean_ident);

                        switch (curkeywtype)
                        {
                            case DIRECTIVE_IFDEF:

                                // Add our new parsing item to queue
                                if (numdefine == 0) // not defined
                                   PutParseInQueue(PARSE_IFDEF, true);
                                else // defined
								{
                                   PutParseInQueue(PARSE_IFDEF, false);
								   deflist[numdefine -1]->is_used = true;
								}

                                scan_status = SCN_STATUS_IDLE;

                                break;

                            case DIRECTIVE_IFNDEF:

                                // Add our new parsing item to queue
                                if (numdefine == 0) // not defined
                                   PutParseInQueue(PARSE_IFNDEF, false);
                                else // defined
                                {
								   PutParseInQueue(PARSE_IFNDEF, true);
								   deflist[numdefine -1]->is_used = true;
								}

                                scan_status = SCN_STATUS_IDLE;

                                break;

                            case DIRECTIVE_UNDEF:

                                if (numdefine == 0) // not defined
                                   PrintSourceWarning(curfile,flinescount,"Nothing to undefine, not defined.");
                                else // defined
                                   UndefineItem(numdefine);

                                scan_status = SCN_STATUS_IDLE;

                                break;

                            case DIRECTIVE_DEFINE:

                                // Add our new define with a "1" default value
								PutDefineInList(clean_ident,"1\0");
                                scan_status = SCN_STATUS_IDLE;
                                break;

                            default:

                                ErrorExit("Invalid 'curkeywtype' in processfile() function!\n");

                        }

                        free(clean_ident);

                        scan_status = SCN_STATUS_IDLE;
                    }
                    else // end of line and empty identifier
                         PrintSourceError(curfile,flinescount,"Define identifier expected.");

                    break;

                case 0x09:
                case ' ':

                    // Get current identifier string
                    for (k = 0; k < (i - scan_offset); k++)
                        curident[k] = memfile[scan_offset + k];

                    if (IsNotEmpty(curident)!=0)
                    {
						char* clean_ident = CleanStr(curident); // PUTA 2020

                        int numdefine = IsDefined(clean_ident);

                        switch (curkeywtype)
                        {
                            case DIRECTIVE_IFDEF:

                                // Add our new parsing item to queue
                                if (numdefine == 0) // not defined
                                   PutParseInQueue(PARSE_IFDEF, true);
                                else // defined
								{
                                   PutParseInQueue(PARSE_IFDEF, false);
								   deflist[numdefine -1]->is_used = true;
								}

                                scan_status = SCN_STATUS_IDLE;
                                break;

                            case DIRECTIVE_IFNDEF:

                                // Add our new parsing item to queue
                                if (numdefine == 0) // not defined
                                   PutParseInQueue(PARSE_IFNDEF, false);
                                else // defined
								{
                                   PutParseInQueue(PARSE_IFNDEF, true);
								   deflist[numdefine -1]->is_used = true;
								}

                                scan_status = SCN_STATUS_IDLE;
                                break;

                            case DIRECTIVE_UNDEF:

                                if (numdefine == 0) // not defined
                                    PrintSourceWarning(curfile,flinescount,"Nothing to undefine, not defined.");
                                else // defined
                                    UndefineItem(numdefine);

                                scan_status = SCN_STATUS_IDLE;
                                break;

                            case DIRECTIVE_DEFINE:

                                scan_status = SCN_STATUS_VALUE; // we need the value string
                                memset(curvalue,0,MAX_ST_SIZE);
                                scan_offset = i + 1;

                                break;

                            default:

                                ErrorExit("Invalid 'curkeywtype' in processfile() function!\n");

                        }

                        free(clean_ident);
                    }
            }
        }

        if ((scan_status == SCN_STATUS_IDLE && !we_r_parsing) || // If we r in idle status and not parsing
            (memfile[i]==0x0A || memfile[i]==0x0D)) // or it is a line break, and keepnewlines pragma is on..
        {
            WriteOutChar(memfile[i]);

            if (memfile[i]==0x0D)
            {
                we_r_lcomented = false;
                flinescount++;
            }
            else if (memfile[i]==0x0A)
            {
                we_r_lcomented = false;

                if (i>0) // not on first char..
                    if (memfile[i-1]!=0x0D) // only counts a line break if we HAVEN'T FOUND a 0x0D first  - (comment before May 2023 was->) - found a 0x0a first
                    {
                        flinescount++;
                        //glinescount++; // Commented out in May 2023 - Global counter for this is increased at the end of this function
                    }
            }
        }
    }

    // CHECK FOR ERRORS FINISHING

    //- Parsing error -//
    if (q_parsenum > 0)
    {
        if (parsequeue[q_parsenum-1]->type == PARSE_IFDEF)
            PrintSourceError(curfile,parsequeue[q_parsenum-1]->on_line,"IFDEF without ENDIF");
        else if (parsequeue[q_parsenum-1]->type == PARSE_IFNDEF)
            PrintSourceError(curfile,parsequeue[q_parsenum-1]->on_line,"IFNDEF without ENDIF");
    }

    // If we are inside a list
    if (scan_status == SCN_STATUS_LIST)
        PrintSourceError(curfile,flinescount,"Not found ENDLIST for INCLUDELIST!");

    // Check for quoted state ending
    if (quote_state)
        PrintSourceWarning(curfile,flinescount,"File ends in quote state.");

    // Check for block commented end
    if (we_r_bcomented)
        PrintSourceWarning(curfile,flinescount,"File ends block-commented.");

    // Update global line counter //
    glinescount += flinescount;
}

//======================================================================
// Outputs the byte to the target memory and expands memory if needed

void WriteOutChar(char data)
{
    if (targetmempos >= targetmemsize) // Do we need more memory for output?
        ExpandTargetMem();

    targetmem[targetmempos] = data;
    targetmempos ++;
}

//==============================================
// prints a string to console output

void PrintIt (char* what)
{
    /*DWORD cWritten;

    if (!WriteFile(hndOut,what,strlen(what),&cWritten,NULL))
    {
        MessageBox(NULL,"Can't write to standard output",ST_BOXERROR,NULL);
        ExitProcess(3);
    }*/

    printf("%s", what);
}

//===============================================
// Puts the given text into the current file

void PrintFile (char* what)
{
    //DWORD cWritten;

    //if (!WriteFile(fhnd,what,strlen(what),&cWritten,NULL))
    //{
    //    ErrorExit("Error writing PROGS.SRC file!");
    //}
    fwrite(what, 1, strlen(what), fhnd);
}

//==================================================================
// We've found something to warn about, report to console output

void PrintSourceWarning (char* thefile, int theline, char* what)
{
    if (fastmode) return;
	
	theline++;

    char tempstr[20];
    sprintf(tempstr,"%d",theline);

    PrintIt(thefile);
    PrintIt(":");
    PrintIt(tempstr);
    PrintIt(":WARNING! ");
    PrintIt(what);
    PrintIt("\n");
}

//=======================================================================
// Reports some information on the code

void PrintSourceInfo (char* thefile, int theline, char* what)
{
    if (fastmode) return;
	
	theline++;

    char tempstr[20];
    sprintf(tempstr,"%d",theline);

    PrintIt(thefile);
    PrintIt(":");
    PrintIt(tempstr);
    PrintIt(":INFO ");
    PrintIt(what);
    PrintIt("\n");
}

//===================================================================
// reports an error on the quakeC source code and exits

void PrintSourceError (char* thefile, int theline, char* what)
{
    theline++;

    char tempstr[20];
    sprintf(tempstr,"%d",theline);

    PrintIt(thefile);
    PrintIt(":");
    PrintIt(tempstr);
    PrintIt(":ERROR! ");
    PrintIt(what);
    PrintIt("\n");

    SourceErrorExit(); // Exits the program
}

//===================================================================
// Expands by MAX_ST_SIZE bytes the memory allocated for output file

void ExpandTargetMem()
{
    targetmem = (char*)realloc(targetmem, targetmemsize + MAX_ST_SIZE); // we need 128 more as max, as value strings can become 127 chars + NULL
    if (targetmem == NULL)
        ErrorExit("Error expanding memory for output file!\n");

    targetmemsize = targetmemsize + MAX_ST_SIZE;
}

//===========================================================================
// reports a program error and exits the program with 2 errorlevel

void ErrorExit(char *msg)
{
    PrintIt("PROGRAM ERROR! ");
    PrintIt(msg);
    PerformCleanUp();
    //ExitProcess(2);
    exit(2);
}

//==================================================================
// Exit program, error in quakeC source code found

void SourceErrorExit()
{
    PrintIt("Can't continue, error found.\n");
    PerformCleanUp();
    //ExitProcess(1);
    exit(1);
}

//============================================================
// performs clean up tasks b4 exit program

void PerformCleanUp ()
{
    unsigned int i;

	// file clean-up?
    if (fhnd != NULL)//!= INVALID_HANDLE_VALUE)
        fclose(fhnd);//CloseHandle(fhnd);

    // Is our file buffer clean?
    if (memfile != NULL)
        free(memfile);
    if (targetmem != NULL)
        free(targetmem);
		
    // Free our file queue items
    for (i = 0; i < MAX_FILE_QUEUE && i < q_filenum; i++)
    {
        if (filequeue[i]!=NULL)
            delete filequeue[i];
    }

    // Free our parse queue items
    for (i = 0; i < MAX_PARSE_QUEUE && i < q_parsenum; i++)
    {
        if (parsequeue[i]!=NULL)
            delete parsequeue[i];
    }

	// Free our define list
    for (i = 0; i < MAX_DEFINES && i < gdefscount; i++)
    {
        if (deflist[i]!=NULL)
            delete deflist[i];
    }
}

//=================================================================
// adds a filename to the queue of files to process

bool PutFileInQueue(char *fname)
{
    // Clean filename to add
	char* clean_fname = CleanStr(fname); // PUTA 2020

    // Is this file already on queue?
    bool alreadyin = false;

    // Search for this file in queue
    for (unsigned int s=0; s < q_filenum; s++)
    {
        if (strcmp(clean_fname,filequeue[s]->filename)==0)
            alreadyin = true;
    }

    // Report warning, and exit function, if file was in queue
    if (alreadyin)
    {
        char tmpstr[MAX_ST_SIZE+28];
        memset(tmpstr,0,MAX_ST_SIZE+28);
        strcat(tmpstr,"Attempt to add \"");
        strcat(tmpstr, clean_fname);
        strcat(tmpstr,"\" to file queue again!");
        PrintSourceWarning(curfile,flinescount,tmpstr);
        free(clean_fname);
        return false;
    }

    // Create new item
    filequeue[q_filenum] = new fileitem;

    // check for memory error..
    if (filequeue[q_filenum] == NULL)
        ErrorExit("Memory error on 'PutFileInQueue()' function!\n");

    // set all string field chars to NULL
    for (int i = 0; i < MAX_ST_SIZE; i++)
    {
        filequeue[q_filenum]->filename[i] = 0;
        filequeue[q_filenum]->invoked_by[i] = 0;
    }

    // apply fields
    strcpy(filequeue[q_filenum]->filename, clean_fname);
    strcpy(filequeue[q_filenum]->invoked_by, curfile); // current file being processed
    filequeue[q_filenum]->on_line = flinescount; // current line being processed
    filequeue[q_filenum]->compile = true;
    // item ready

    q_filenum++; // we added a file to the queue!
    filecount++; // files added from the current source file

    if (q_filenum >= MAX_FILE_QUEUE) // we reached max?
        ErrorExit("Maximum file queue reached! (256 files max)\n");

    free(clean_fname);
    return true;
}

//=================================================================
// adds a new define item to the list of defines

void PutDefineInList(char* ident, char* value)
{
    // clean up strings
	char* clean_ident = CleanStr(ident); // PUTA 2020
	char* clean_value = CleanStr(value); // PUTA 2020

    // Check for redefines
    defineitem* theitem;
    int numdefine = IsInDefList(clean_ident); // is it in defines list? (even if undefined)

    if (numdefine == 0) // Not defined, we need a new one
    {
        deflist[gdefscount] = new defineitem; // Allocate our new item in memory
        theitem = deflist[gdefscount];

		//set our initial define state
		memset(theitem->defined_by,0,MAX_ST_SIZE);
		strcpy(theitem->defined_by,curfile);
		theitem->is_used = false;
		theitem->on_line = flinescount;

		gdefscount++; // we added a define
    }
    else
    {   // we got a match, make the pointer go for it
        theitem = deflist[numdefine - 1]; // IsDefined does a return++

		// report redefinition warning, if enabled
        if (check_redefines)
        {
            char tstr[MAX_ST_SIZE + 32+3];
            tstr[0] = '\0';
            strcat(tstr, "Value for identifier \"");
            strcat(tstr, deflist[numdefine - 1]->identifier);
            strcat(tstr, "\" redefined.");
            //PrintSourceWarning(curfile, flinescount, "Value for identifier redefined.");
            PrintSourceWarning(curfile, flinescount, tstr);
        }
    }

    // check for memory error or wrong pointer..
    if (theitem == NULL)
    {
        free(clean_ident);
        free(clean_value);
        ErrorExit("Memory error or wrong pointer operation on 'PutDefineInList()' function!\n");
    }

    // set all string field chars to NULL
    for (unsigned int l = 0; l < MAX_ST_SIZE; l++)
    {
        theitem->value[l] = 0;
        theitem->identifier[l] = 0;
    }

	//2020 - Original was ParseValue(value); ALL ALONE
	char tmpstr[MAX_ST_SIZE];
	char* pointer;
	pointer = ParseValue(clean_value);
    strcpy(tmpstr,pointer);
	
    // apply fields
    theitem->is_defined = true;
    strcpy(theitem->identifier, clean_ident);
    strcpy(theitem->value,tmpstr); // was value
    // item ready    

	// 2020 Free memory got from parsevalue
	free(pointer);

    // December 2023
    free(clean_ident);
    free(clean_value);

    if (gdefscount >= MAX_DEFINES)
        ErrorExit("Max number of defines reached!\n"); // Added \n newline on May 2023

    fdefscount++; // increase file defs counter too!
}

//======================================================================================
// Marks as undefined an item on the defines list, returns false if not successfull

void UndefineItem(unsigned int numdefine)
{
    numdefine--; // compensate IsDefined return++;

    if (numdefine >= gdefscount)
        ErrorExit("Define location is beyond array in 'undefineItem()' function.\n");

    if (deflist[numdefine]->is_defined == false)
        PrintSourceWarning(curfile,flinescount,"Already undefined.");

    deflist[numdefine]->is_defined = false;

	deflist[numdefine]->is_used = true; // TODO: make this better
}

//=================================================================
// adds a new parse item to the queue

void PutParseInQueue(parse_type type, bool causes_parse)
{
    parsequeue[q_parsenum] = new parseitem;

    // check for memory error..
    if (parsequeue[q_parsenum] == NULL)
        ErrorExit("Memory error on 'PutParseInQueue()' function!\n");

    // apply fields
    parsequeue[q_parsenum]->type = type; // IFDEF or IFNDEF (for error reporting purposses only)
    parsequeue[q_parsenum]->causes_parse = causes_parse; // does it make to exclude source code?
    parsequeue[q_parsenum]->inversed = false; // this is set to TRUE if an ELSE is found
    parsequeue[q_parsenum]->on_line = flinescount; // current line beeing processed
    // item ready

    q_parsenum++; // we added an IFDEF or IFNDEF to the parse queue!

    if (q_parsenum >= MAX_PARSE_QUEUE) // we reached max?
        PrintSourceError(curfile,flinescount,"Maximum parse depth reached! (16 consecutive IFDEF/IFNDEF's max)");

    // Update globals for parsing here
    we_r_parsing = causes_parse; // DONE?
}

//===================================================================================================
// We've just found an ELSE, so we need to inverse parsing globals and update parse queue

void InvertParseItem()
{
    // Check errors
    if (q_parsenum <= 0) // ELSE without an IFDEF or IFNDEF?
       PrintSourceError(curfile,flinescount,"ELSE without IFDEF/IFNDEF");

    if (parsequeue[q_parsenum - 1]->inversed == true) // already inversed (DOUBLE ELSE)
        PrintSourceError(curfile,flinescount,"Consecutive ELSE");

    // Ok, update status and queue item..
    we_r_parsing = !we_r_parsing; // invert current status
    parsequeue[q_parsenum - 1]->inversed = true;
}

//===================================================================================================
// We've just found an ENDIF, we resolved last IFDEF or IFNDEF so we need to update parse queue
// and update parsing globals

void ResolveParseItem()
{
    // Exit on error
    if (q_parsenum < 1)
        PrintSourceError(curfile,flinescount,"ENDIF without IFDEF/IFNDEF");

    delete parsequeue[q_parsenum - 1]; // remove from memory
    parsequeue[q_parsenum - 1] = NULL; // resets pointer

    q_parsenum--; // we resolved an IFDEF or IFNDEF from the parse queue!

    if (q_parsenum <= 0) // we are not under any IFDEF or IFNDEF! cool!! :)
    {
        we_r_parsing = false;
    }
    else // we went a step below in our parsing queue
    {
        if (parsequeue[q_parsenum - 1]->inversed) // are we inversed with an ELSE ?
            we_r_parsing = !(parsequeue[q_parsenum - 1]->causes_parse);
        else
            we_r_parsing = parsequeue[q_parsenum - 1]->causes_parse;
    }
}

//========================================================================================================
// Gets type of keyword that correspong to the string marked by the offset on memfile, and size of bytes

int GetKeywordType(int curpos)
{
    if ((curpos - scan_offset) > MAX_ST_SIZE - 1)
        PrintSourceError(curfile,flinescount,"Keyword too long! (max 512 chars)");

    char tkey[MAX_ST_SIZE];
    memset(tkey, 0, MAX_ST_SIZE); // Fill it all with NULL

	// 2020 Moved z declaration before for loop
	unsigned int z;

    // Create the NULL-terminated string, with the supposed keyword in
    for (z=0; z < (curpos - scan_offset); z++)
    {
        tkey[z] = memfile[scan_offset + z];
    }

	char* clean_key = CleanStr(tkey); // PUTA 2020

    int type =GetDirectiveType(clean_key);

    if (we_r_parsing && type == DIRECTIVE_IDENT)
    {
        free(clean_key);
        return type;
    }

    if (we_r_parsing && type != DIRECTIVE_ENDIF && type != DIRECTIVE_ELSE && type != DIRECTIVE_IFDEF && type != DIRECTIVE_IFNDEF)
    {
        free(clean_key);
        return DIRECTIVE_IDENT;
    }

    if (type != DIRECTIVE_IDENT)
    {
        free(clean_key);
        return type;
    }

    // not a directive...

    // so should be a defined value, let's search for it and write out to output if so
    int numdef = IsDefined(clean_key);

    if (numdef == 0) // not on defines list
    {
        char tempstr[MAX_ST_SIZE + 35];
        tempstr[0] = '\0';
        strcat(tempstr,"Unknown identifier, \"");
        strcat(tempstr, clean_key);
        strcat(tempstr,"\" not defined!");

        free(clean_key);

        PrintSourceError(curfile,flinescount,tempstr);
    }
    else // it's a define, output real value to memtarget
    {
        numdef--; // cause isDefined returns the item + 1

        for (z = 0; z < strlen(deflist[numdef]->value); z++)
            WriteOutChar(deflist[numdef]->value[z]);

		// Set the new state on define used
		deflist[numdef]->is_used = true;
    }

    free(clean_key);

    return DIRECTIVE_IDENT;
}

bool DirectiveWantsData(int tkeyw)
{
    switch (tkeyw)
    {
        case DIRECTIVE_IFDEF:
        case DIRECTIVE_IFNDEF:
        case DIRECTIVE_UNDEF:
        case DIRECTIVE_INCLUDE:
        case DIRECTIVE_DEFINE:
        case DIRECTIVE_PRAGMA:

            return true;

        default:

            return false;
    }
}

//========================================================================================================
// Gets type of PRAGMA that correspond to the string marked by the offset on memfile, and size of bytes

int GetPragmaType(int curpos)
{
    if ((curpos - scan_offset) > MAX_ST_SIZE - 1)
        PrintSourceError(curfile,flinescount,"Pragma identifier way too long! (max 512 chars)");

    char tkey[MAX_ST_SIZE];
    memset(tkey, 0, MAX_ST_SIZE); // Fill it all with NULL

    // Create the NULL-terminated string, with the supposed pragma in
    for (unsigned int z=0; z < (curpos - scan_offset); z++)
    {
        tkey[z] = memfile[scan_offset + z];
    }

    // Check if current string is empty still
    if (IsNotEmpty(tkey)==0)
        return PRAGMA_EMPTYSTILL;

    // Clean spaces and tabs between directive and pragma identifier
	char* clean_key = CleanStr(tkey); // PUTA 2020

    //- START STRING COMPARISONS TO GET TYPE OF PRAGMA -//
    if (strlen(clean_key) == strlen(ST_PRAGMA_COMPILE))
    {
        if (strcmp(clean_key, ST_PRAGMA_COMPILE) == 0)
        {
            free(clean_key);
            return PRAGMA_COMPILE;
        }
    }

    if (strlen(clean_key) == strlen(ST_PRAGMA_NOCOMPILE))
    {
        if (strcmp(clean_key, ST_PRAGMA_NOCOMPILE) == 0)
        {
            free(clean_key);
            return PRAGMA_NOCOMPILE;
        }
    }

	if (strlen(clean_key) == strlen(ST_PRAGMA_CHECKREDEFS_ON))
	{
        if (strcmp(clean_key, ST_PRAGMA_CHECKREDEFS_ON) == 0)
        {
            free(clean_key);
            return PRAGMA_CHECKREDEFS_ON;
        }
	}

	if (strlen(clean_key) == strlen(ST_PRAGMA_CHECKREDEFS_OFF))
	{
        if (strcmp(clean_key, ST_PRAGMA_CHECKREDEFS_OFF) == 0)
        {
            free(clean_key);
            return PRAGMA_CHECKREDEFS_OFF;
        }
	}

	if (strlen(clean_key) == strlen(ST_PRAGMA_CHECKUNUSED_ON))
	{
        if (strcmp(clean_key, ST_PRAGMA_CHECKUNUSED_ON) == 0)
        {
            free(clean_key);
            return PRAGMA_CHECKUNUSED_ON;
        }
	}

	if (strlen(clean_key) == strlen(ST_PRAGMA_CHECKUNUSED_OFF))
	{
        if (strcmp(clean_key, ST_PRAGMA_CHECKUNUSED_OFF) == 0)
        {
            free(clean_key);
            return PRAGMA_CHECKUNUSED_OFF;
        }
	}
	
    // January 2024
    if (strlen(clean_key) == strlen(ST_PRAGMA_PROGSSRC))
    {
        if (strcmp(clean_key, ST_PRAGMA_PROGSSRC) == 0)
        {
            free(clean_key);
            return PRAGMA_PROGSSRC;
        }
    }

    if (strlen(clean_key) == strlen(ST_PRAGMA_PROGSDAT))
    {
        if (strcmp(clean_key, ST_PRAGMA_PROGSDAT) == 0)
        {
            free(clean_key);
            return PRAGMA_PROGSDAT;
        }
    }
    // End January 2024

    /*else if (strlen(tkey) == strlen(ST_PRAGMA_PROGSSRC))
    {
        if (memcmp(tkey,ST_PRAGMA_PROGSSRC,strlen(ST_PRAGMA_PROGSSRC) == 0)) // same string
            return PRAGMA_PROGSSRC;
    }
    else if (strlen(tkey) == strlen(ST_PRAGMA_PROGSDAT))
    {
        if (memcmp(tkey,ST_PRAGMA_PROGSDAT,strlen(ST_PRAGMA_PROGSDAT) == 0)) // same string
            return PRAGMA_PROGSDAT;
    } */

    free(clean_key);

    // not a supported pragma...
    return PRAGMA_UNSUPPORTED;
}

/*bool PragmaWantsData(int tkeyw)
{
    switch (tkeyw)
    {
        case PRAGMA_PROGSDAT:
        case PRAGMA_PROGSSRC:

            return true;

        default:

            return false;
    }
}*/

//=========================================================================================
// We've just found a pragma keyword, apply stuff
// changes status, updates globals, add new parse items.. etc.-

void ExecutePragma(int curpos)
{
    switch (curpragmatype)
    {
        // PRAGMAS THAT DOESNT REQUIRE ANY PARAM - THEY DO SOMETHING BY THEIRSELVES //
        case PRAGMA_COMPILE:

            filequeue[q_filepos]->compile=true;
            scan_status = SCN_STATUS_IDLE;
            PrintSourceInfo(curfile,flinescount,"File marked to COMPILE");
			break;

        case PRAGMA_NOCOMPILE:

            filequeue[q_filepos]->compile=false;
            scan_status = SCN_STATUS_IDLE;
			PrintSourceInfo(curfile,flinescount,"File marked to NOT COMPILE");
            break;

		case PRAGMA_CHECKREDEFS_ON:

			check_redefines = true;
			PrintSourceInfo(curfile,flinescount,"Redefinition check is turned ON");
			scan_status = SCN_STATUS_IDLE;
			break;

		case PRAGMA_CHECKREDEFS_OFF:

			check_redefines = false;
			PrintSourceInfo(curfile,flinescount,"Redefinition check is turned OFF");
			scan_status = SCN_STATUS_IDLE;
			break;

		case PRAGMA_CHECKUNUSED_ON:
			
			check_unused = true;
			PrintSourceInfo(curfile,flinescount,"Unused items check is turned ON");
			scan_status = SCN_STATUS_IDLE;
			break;

		case PRAGMA_CHECKUNUSED_OFF:
			
			check_unused = false;
			PrintSourceInfo(curfile,flinescount,"Unused items check is turned OFF");
			scan_status = SCN_STATUS_IDLE;
			break;

        // Require a quoted string (filename)
            // CODE BLOCK ENABLED IN January 2024
        case PRAGMA_PROGSSRC:
        case PRAGMA_PROGSDAT:

            scan_status = SCN_STATUS_PRAGMAVALUE;
            scan_offset = curpos;
            break;
            // End of January 2024

        default:

            ErrorExit("Unknown PRAGMA in 'ExecutePragma()' function!\n");
    }
}

//================================================================================
// returns the number of define (+1) in queue array if the string is a current define
// returns 0 if ident was not defined

unsigned int IsDefined(char* ident)
{
    // Clean empty limits on string edges
	char* clean_ident = CleanStr(ident); // PUTA 2020

    // Is there any defined values?
    if (gdefscount <= 0)
        return 0;// nope, so.. not defined

    // See all defines for a matching one:
    for (unsigned int d = 0; d < gdefscount; d++)
    {
        if (deflist[d] == NULL)
        ; // FIXME: why the fuck this??
        else
        if (deflist[d]->is_defined) // if we rnt undefined
            if (strcmp(deflist[d]->identifier, clean_ident) == 0) // matches string
            {
                free(clean_ident);
                return d + 1;
            }
    }

    free(clean_ident);

    return 0;
}

unsigned int IsInDefList(char* ident)
{
    // Clean empty limits on string edges
	char* clean_ident = CleanStr(ident); // PUTA 2020

    // Is there any defined values?
    if (gdefscount <= 0)
        return 0;// nope, so.. not defined

    // See all defines for a matching one:
    for (unsigned int d = 0; d < gdefscount; d++)
    {
        if (deflist[d] == NULL)
        ; // FIXME:
        else
            if (strcmp(deflist[d]->identifier, ident) == 0) // matches string
            {
                free(clean_ident);
                return d + 1;
            }
    }

    free(clean_ident);

    return 0;
}

//=========================================================================================
// We've just found a directive keyword, apply stuff
// changes status, updates globals, add new parse items.. etc.-

void ExecuteDirective(int curpos)
{
    switch (curkeywtype)
    {
        // DIRECTIVES THAT DOESNT REQUIRE ANY PARAM - THEY DO SOMETHING BY THEIRSELVES //
        case DIRECTIVE_ENDIF:

            if (parse_stack > 0)
            {
                parse_stack--;
                scan_status = SCN_STATUS_IDLE;
                return;
            }

            ResolveParseItem();
            scan_status = SCN_STATUS_IDLE;
            break;

        case DIRECTIVE_ELSE:

            if (parse_stack > 0)
            {
                scan_status = SCN_STATUS_IDLE;
                return;
            }

            InvertParseItem();
            scan_status = SCN_STATUS_IDLE;
            break;

        case DIRECTIVE_INCLUDELIST:

            //TODO: Make directives on includelist possible
            /*if (scan_status == SCN_STATUS_LIST)
                PrintSourceError(curfile,flinescount,"Already on a INCLUDELIST!");*/

			PrintSourceInfo(curfile,flinescount,"Include list found");

            scan_status = SCN_STATUS_LIST;
            memset(curvalue,0,MAX_ST_SIZE);
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_ENDLIST:

            // Error cause endlist is checked in another place
            PrintSourceError(curfile,flinescount,"ENDLIST without INCLUDELIST");
            scan_status = SCN_STATUS_IDLE;
            break;

        // DIRECTIVES THAT REQUIRE SOME KIND OF DATA - UPDATE STATUS //
        case DIRECTIVE_IFDEF:

            if (we_r_parsing)
            {
                parse_stack++;
                scan_status = SCN_STATUS_IDLE;
                return;
            }

            memset(curident,0,MAX_ST_SIZE); // Reset our identifier string
            scan_status = SCN_STATUS_DEFINE;
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_IFNDEF:

            if (we_r_parsing)
            {
                parse_stack++;
                return;
            }

            memset(curident,0,MAX_ST_SIZE); // Reset our identifier string
            scan_status = SCN_STATUS_DEFINE;
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_DEFINE:

            memset(curident,0,MAX_ST_SIZE); // Reset our identifier string
            scan_status = SCN_STATUS_DEFINE;
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_UNDEF:

            memset(curident,0,MAX_ST_SIZE); // Reset our identifier string
            scan_status = SCN_STATUS_DEFINE;
            scan_offset = curpos + 1;
            break;

        // Enabled in January 2024
        case DIRECTIVE_INCLUDE:

            memset(curident, 0, MAX_ST_SIZE);
            memset(curvalue, 0, MAX_ST_SIZE);
            scan_status = SCN_STATUS_INCLUDE;
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_PRAGMA:

            scan_status = SCN_STATUS_PRAGMA;
            scan_offset = curpos + 1;
            break;

        case DIRECTIVE_IDENT:

            // we've just replaced a define, we should restore status
            scan_status = SCN_STATUS_IDLE;
            break;

        default:

            ErrorExit("Unknown directive in 'ExecuteDirective()' function!\n");
    }
}

//======================================================================================
// returns TRUE if char do currently is a separator

bool CharIsSeparator(char tchar)
{
	if (tchar == 0x09) // TAB is a separator too
		return true;
	if (tchar == ' ')
		return true;
	if (tchar == '!')
		return true;
	if (tchar == '*')
		return true;
	if (tchar == '+')
		return true;
	if (tchar == '-')
		return true;
	if (tchar == '=')
		return true;
	if (tchar == ')')
		return true;
	if (tchar == '(')
		return true;
	if (tchar == '/')
		return true;
	if (tchar == ',')
		return true;
	if (tchar == '.')
		return true;
	if (tchar == '>')
		return true;
	if (tchar == '<')
		return true;
	if (tchar == '{')
		return true;
	if (tchar == '}')
		return true;
	if (tchar == '\"')
		return true;
	if (tchar == '\\') // From here below added on 1.7 - 2020
		return true;
	if (tchar == '[')
		return true;
	if (tchar == ']')
		return true;
	if (tchar == ';')
		return true;
	if (tchar == '&')
		return true;
	if (tchar == '|')
		return true;

    return false;
}
/*
// new line chars
bool CharIsNewLine(char tchar)
{
    if (tchar == 0x0D || tchar == 0x0A)
        return true;

    return false;
}*/

//======================================================================
// returns the length of string excluding spaces and TAB chars

int IsNotEmpty(char* tstr)
{
    if (strlen(tstr) == 0) return 0;

    int numvalid = 0; // valid chars on string

    for (unsigned int i=0; i < strlen(tstr); i++)
    {
        if (tstr[i]!=' ' && tstr[i]!=0x09 && tstr[i]!=0x0A && tstr[i]!=0x0D)
            numvalid++;
    }

    return numvalid;
}

//==========================================================================
// Cuts the spaces and tabs from the string

char* CleanStr(char* tstr)
{
	/* Original code before December 2023
    int len = strlen(tstr);

	if (len <= 0)
		return "";

	char* returnval = (char*)malloc(MAX_ST_SIZE);
	
	char tempstr[MAX_ST_SIZE];
    memset(tempstr,NULL,MAX_ST_SIZE);
	
	unsigned int pos1 = 0;
    unsigned int pos2 = len; //2020 suggestion -1 ?
    
    // get starting pos
    while (IsEmptyChar(tstr[pos1]) && pos1 < MAX_ST_SIZE - 1)
        pos1++;
    
    // get final pos
    while ((IsEmptyChar(tstr[pos2]) || tstr[pos2]== NULL) && pos2 > 0)
        pos2--;
    
    // Parse string into limits 
    for (unsigned int a=pos1; a < pos2 + 1; a++)
        tempstr[a - pos1] = tstr[a];
    	
	//strcpy_s(returnval,MAX_ST_SIZE, tempstr);
    strcpy(returnval, tempstr);
	return returnval;
    */

    // New code on December 2023
    int len = strlen(tstr);

    //if (len <= 0)
      //  return NULL;//tstr;

    char* returnval = (char*)malloc(MAX_ST_SIZE);

    char tempstr[MAX_ST_SIZE];
    memset(tempstr, 0, MAX_ST_SIZE);

    unsigned int pos1 = 0;
    unsigned int pos2 = len; //2020 suggestion -1 ?

    // get starting pos
    while (IsEmptyChar(tstr[pos1]) && pos1 < MAX_ST_SIZE - 1)
        pos1++;

    // get final pos
    while ((IsEmptyChar(tstr[pos2]) || tstr[pos2] == '\0') && pos2 > pos1)
        pos2--;

    // Parse string into limits 
    unsigned int a;
    for (a = pos1; a < pos2 + 1; a++)
        tempstr[a - pos1] = tstr[a];

    //strcpy_s(returnval,MAX_ST_SIZE, tempstr);
    strcpy(returnval, tempstr);

    return returnval;
}

//=================================================================================================
// returns true, if the string following the '#' on current scanning pos is an ENDLIST directive

bool IsValidEndList(unsigned int curpos)
{
    // check for errors
    if (curpos + 1 + 7 > fsize) // if check goes beyond file..
        return false;

    char thestr[8];

    // Make the input string on memfile
    for (int i=0; i < 7; i++)
    {
        thestr[i] = memfile[curpos + 1 + i];
    }

    // make it null-terminated
    thestr[7] = '\0';

    // String matches?
    if (strcmp(thestr,ST_DIR_ENDLIST)==0)
        return true;
    else
        return false;
}

//=============================================================================
// returns true if the char should be removed when cleaning a string

bool IsEmptyChar(char tchar)
{
    if (tchar == 0x09 || tchar == 0x0D || tchar == 0x0A || tchar == ' ')
        return true;

    return false;
}

//=======================================================================================
// Arranges our string to put out any comments and translate defines in define values

char* ParseValue(char* tvalue)
{
	// 2020
	char* returnval = (char*)malloc(MAX_ST_SIZE);

    int wherecomment = 999; // no where

    char finalstr[MAX_ST_SIZE*2];
    memset(finalstr,0,MAX_ST_SIZE*2);

	// 2020 Moved i declaration before for loop
	int i;

    // find begining of comments
    bool done = false;
    for (i=0; i < MAX_ST_SIZE - 1 && !done; i++)
    {
        if (tvalue[i]=='/')
            if (tvalue[i+1]=='/')
            {
                wherecomment = i;
                done = true;
            }
    }

    // Remove comments
    for (i=wherecomment; i < MAX_ST_SIZE; i++)
        tvalue[i]='\0';

    ///strcpy(tvalue, CleanStr(tvalue));        
	char* clean_value = CleanStr(tvalue); // PUTA 2020

    // Replace defines with their value
    for (i=0; i < MAX_ST_SIZE - 1 && clean_value[i] != '\0'; i++)
    {
        if (clean_value[i]=='#') // found a keyword?
        {

            // get the end of keyword
            int z;
            for (z=i+1; true; z++)
            {
				if (IsEmptyChar(clean_value[z]) != false)
					break;
				if (CharIsSeparator(clean_value[z]) != false)
					break;
				if (clean_value[z] == '\0')
					break;
				if (z >= MAX_ST_SIZE - i -1)
					break;
                if (clean_value[z] == '#')
                    PrintSourceError(curfile,flinescount,"Illegal use of '#' char.");
            }

            // z-- FIXME: wtf
            z--;

            // Check errors
            if (z == i + 1) // empty keyword
                PrintSourceError(curfile,flinescount,"Empty keyword on define value.");

            // get the identifier string
            char ident[MAX_ST_SIZE];
            memset(ident,0,MAX_ST_SIZE);
            int x;
            for (x=i+1; x<(z+1); x++)
                ident[x-i-1] = clean_value[x];

            // make sure it isnt a reserved word
            if (GetDirectiveType(ident)!=DIRECTIVE_IDENT)
                PrintSourceError(curfile,flinescount,"Illegal use of preprocessor reserved word.");

            // Update main char loop position
            i = i + strlen(ident);

            int numdef = IsDefined(ident);
            if (numdef == 0)
            {
                PrintSourceError(curfile,flinescount,"Undefined symbol as define value.");
            }
            else // Add it to our final string
            {
                // set to true the is_used flag
				deflist[numdef-1]->is_used = true;

				strcat(finalstr,deflist[numdef-1]->value);
                if (strlen(finalstr) >= MAX_ST_SIZE - 1)
                    PrintSourceError(curfile,flinescount,"Resulting string value exceeds char limit! (512)");
            }
        }
        else // Regular non-parse'able character happening, coming in...
        {
            char tmpstr[2];
            tmpstr[0]= clean_value[i];
            tmpstr[1]='\0';
            strcat(finalstr,tmpstr);

            // Exceeds string limit max?
            if (strlen(finalstr) >= MAX_ST_SIZE - 1)
                PrintSourceError(curfile,flinescount,"Resulting string value exceeds char limit! (512)");
        }
    }

    //strcpy_s(returnval,MAX_ST_SIZE, CleanStr(finalstr));

    free(clean_value);

    clean_value = CleanStr(finalstr);

    strcpy(returnval, clean_value);
    free(clean_value);

    //strcpy(returnval, CleanStr(finalstr));
	return returnval;
}

//===================================================================================================
// returns the directive ID number for the string passed, if none, DIRECTIVE_IDENT is returned

int GetDirectiveType(char* tkey)
{
    //- START STRING COMPARISONS TO GET TYPE OF DIRECTIVE -//
    if (strlen(tkey) == strlen(ST_DIR_IFDEF))
    {
        if (strcmp(tkey,ST_DIR_IFDEF)==0)
            return DIRECTIVE_IFDEF;
    }

    if (strlen(tkey) == strlen(ST_DIR_IFNDEF))
    {
        if (strcmp(tkey,ST_DIR_IFNDEF)==0)
            return DIRECTIVE_IFNDEF;
    }

    if (strlen(tkey) == strlen(ST_DIR_ELSE))
    {
        if (strcmp(tkey,ST_DIR_ELSE)==0)
            return DIRECTIVE_ELSE;
    }

    if (strlen(tkey) == strlen(ST_DIR_ENDIF))
    {
        if (strcmp(tkey,ST_DIR_ENDIF)==0)
            return DIRECTIVE_ENDIF;
    }

    if (strlen(tkey) == strlen(ST_DIR_DEFINE))
    {
        if (strcmp(tkey,ST_DIR_DEFINE)==0)
            return DIRECTIVE_DEFINE;
    }

    if (strlen(tkey) == strlen(ST_DIR_UNDEF))
    {
        if (strcmp(tkey,ST_DIR_UNDEF)==0)
            return DIRECTIVE_UNDEF;
    }

    if (strlen(tkey) == strlen(ST_DIR_PRAGMA))
    {
        if (strcmp(tkey,ST_DIR_PRAGMA)==0)
            return DIRECTIVE_PRAGMA;
    }

    if (strlen(tkey) == strlen(ST_DIR_INCLUDELIST))
    {
        if (strcmp(tkey,ST_DIR_INCLUDELIST)==0)
            return DIRECTIVE_INCLUDELIST;
    }

    if (strlen(tkey) == strlen(ST_DIR_ENDLIST))
    {
        if (strcmp(tkey,ST_DIR_ENDLIST)==0)
            return DIRECTIVE_ENDLIST;
    }

    if (strlen(tkey) == strlen(ST_DIR_INCLUDE))
    {
        if (strcmp(tkey,ST_DIR_INCLUDE)==0)
            return DIRECTIVE_INCLUDE;
    }

    return DIRECTIVE_IDENT;
}

//========================================================================
// convert xxxxx.qc into xxxxx.qcp

char* ChangeFilename(char *fname)
{
    char* tmpname = (char*)malloc(MAX_ST_SIZE);
    int i;
	
	memset(tmpname,0,MAX_ST_SIZE);
	    
    for (i=0; i < MAX_ST_SIZE - 1 && fname[i]!='.'; i++)
        tmpname[i] = fname[i];

    tmpname[i]='\0';

    #ifndef _WIN32
    if (fname[strlen(fname)-1] == 'C')
        strcat(tmpname, ".QCp");
    else
    #endif
        strcat(tmpname, ".qcp");

	return tmpname;
}

//========================================================================
// At the end of main program execution, report any unused defines

void CheckUnused()
{
	if (fastmode) return;

	char tstr[MAX_ST_SIZE+21];
	PrintIt("------------------------------------------------\nChecking unused items...\n");
	unsigned int counter = 0;

	int z;

	// Lets scan for unused stuff
	for (unsigned int i = 0; i < gdefscount; i++)
	{		
		if (!deflist[i]->is_used) //Unused?
		{			
			// Skip default definitions
			z = 0;

			while (defines_c[z])
			{
				if (!strcmp(defines_c[z],deflist[i]->identifier))
				{
					z = 999999;
					break;
				}

				z++;
			}

			if (z!=999999)
			{
				tstr[0] = '\0';
				strcpy(tstr,"Definition \"");
				strcat(tstr,deflist[i]->identifier);
				strcat(tstr,"\" is never used");
				PrintSourceWarning(deflist[i]->defined_by,deflist[i]->on_line,tstr);
				counter ++;
			}
		}
	}
	
	// Report results
	if (counter > 0)
	{
		sprintf(tstr,"%d",counter);
		PrintIt("Unused items: ");
		PrintIt(tstr);
		PrintIt(" defines.\n");
	}
	else
	{
		PrintIt("No unused defines.\n");
	}
}

//==================================================
// Formats the date string

void GetFormattedDate()
{
	_date[0]='\0';
	
	time_t t;
	
	time(&t);

	/*tm* now = new tm;
	localtime_s(now, &t);
	
	int year = now->tm_year + 1900;
	int month = now->tm_mon + 1;
	int day = now->tm_mday;*/
#ifdef _WIN32
    struct tm now;
    localtime_s(&now, &t);

    int year = now.tm_year + 1900;
    int month = now.tm_mon + 1;
    int day = now.tm_mday;
#else
    struct tm* now;// = new tm;
    now = localtime(&t);

    int year = now->tm_year + 1900;
    int month = now->tm_mon + 1;
    int day = now->tm_mday;
#endif

	char str_year[6];
	char str_month[4];
	char str_day[4];

	sprintf(str_year, "%d", year);
	sprintf(str_month, "%02d", month);
	sprintf(str_day, "%02d", day);
	
	strcat(_date, "�");
	strcat(_date, str_day);
	strcat(_date, "�");
	strcat(_date, str_month);
	strcat(_date, "�");
	strcat(_date, str_year);
	strcat(_date, "�");	

	PrintIt("Runtime system local date is: [");
	PrintIt(str_day);
	PrintIt("/");
	PrintIt(str_month);
	PrintIt("/");
	PrintIt(str_year);
	PrintIt("]\n");

	//delete now;
}

//=====================================================
// Formats the time string

void GetFormattedTime()
{
	_time[0] = '\0';
	
	time_t t;

	time(&t);
    /*
	tm* now = new tm;

#ifdef _WIN32
	localtime_s(now, &t);
	//now = localtime(&t);
#else
	now = localtime(&t);
#endif
	int hour = now->tm_hour;
	int minutes = now->tm_min;
	int seconds = now->tm_sec;
*/
#ifdef _WIN32

    struct tm now;// = new tm;
    localtime_s(&now, &t);
    //now = localtime(&t);

    int hour = now.tm_hour;
    int minutes = now.tm_min;
    int seconds = now.tm_sec;
#else
    struct tm* now;// = new tm;
    now = localtime(&t);

    int hour = now->tm_hour;
    int minutes = now->tm_min;
    int seconds = now->tm_sec;
#endif

	// FIXME: Something like this needed?
	//if (seconds == 60)
	//	seconds = 0;

	char str_hour[4];
	char str_minutes[4];
	char str_seconds[4];
	
	sprintf(str_hour, "%02d", hour);
	sprintf(str_minutes, "%02d", minutes);
	sprintf(str_seconds, "%02d", seconds);
	
	strcat(_time, "� ");
	strcat(_time, str_hour);
	strcat(_time, "�");
	strcat(_time, str_minutes);
	strcat(_time, "�");
	strcat(_time, str_seconds);
	strcat(_time, " �");

	PrintIt("Runtime system local time is: [ ");
	PrintIt(str_hour);
	PrintIt(":");
	PrintIt(str_minutes);
	PrintIt(":");
	PrintIt(str_seconds);
	PrintIt(" ]\n");

	//delete now;
}

//==========================================================================================================
// Cleans up the pragma dat or src value string

char* CleanUpPragmaStr(char* value)
{
    char* result = (char*)malloc(MAX_ST_SIZE);

    memset(result, 0, MAX_ST_SIZE);

    unsigned int start_str, end_str;

    start_str = 0;
    
    while (IsEmptyChar(value[start_str]) && start_str < MAX_ST_SIZE - 1)
        start_str++;

    if (value[start_str] != '\"')
        PrintSourceError(curfile, flinescount, "Bad format for progs pragma quoted string value.");

    start_str++;

    end_str = start_str;

    while (value[end_str] != '\"' && end_str < MAX_ST_SIZE - 1)
        end_str++;

    if (value[end_str] != '\"')
        PrintSourceError(curfile, flinescount, "Bad format for progs pragma quoted string value.");

    for (unsigned int i = start_str; i < end_str; i++)
        result[i - start_str] = value[i];
    
    return result;
}

//==============================================================================================================
// Cleans up/parses an INCLUDE item string value

char* CleanUpIncludeStr(char* value)
{
    char* result = (char*)malloc(MAX_ST_SIZE);

    memset(result, 0, MAX_ST_SIZE);

    bool quoted = false;

    unsigned int pos1 = 0;
    unsigned int pos2 = strlen(value);

    unsigned int z;

    // Detemine where the comment (if any) begins...
    for (z = pos2 - 1; z > 0; z--)
    {
        if (value[z] == '/')
        {
            if (value[z - 1] == '/')
            {
                z--;
                break;
            }
        }
    }

    if (z > 0)
        pos2 = z;

    // Determine start pos
    for (z = 0; z < pos2; z++)
    {
        if (!IsEmptyChar(value[z]))
        {
            if (value[z] == '\"')
            {
                quoted = true;
                z++;
            }

            break;
        }
    }

    pos1 = z;

    bool success = false;

    // Determine end pos
    for (z = z; z < pos2+1; z++)
    {
        if (IsEmptyChar(value[z]))
        {
            success = true;
            break;            
        }

        if (value[z+1] == '\0')
        {
            z++;
            success = true;
            break;
        }
        
        if (quoted)
        {
            if (value[z+1] == '\"')
            {
                z++;
                success = true;
                break;
            }
        }
        else // We didn't find a starting quote...
        {
            if (z == pos2)
            {
                success = true;
                break;
            }
            else if (value[z] == '\"')
            {
                PrintSourceError(curfile, flinescount, "Unexpected \" character in filename item.");
            }
        }
    }

    pos2 = z;

    if (!success)
        PrintSourceError(curfile, flinescount, "Error in include item string value.");

    for (z = pos1; z < pos2; z++)
    {
        result[z - pos1] = value[z];
    }

    result[z] = '\0';

    return result;
}
